#! /usr/bin/perl

#環境構築
#Debian(Ubuntu) で PerlモジュールをインストールしたいときはCPANを使わなくても良い
#http://server-setting.info/debian/perl-module-install-apt.html
#aptitude install libwww-curl-perl
#aptitude install libdate-simple-perl
#aptitude install libjson-perl

use feature ':5.10';
use utf8;
use Date::Simple;
use Encode;
use WWW::Curl::Easy;
use Data::Dumper;
use JSON;
#use URI::Escape;
#use MIME::Base64;

#P2P地震情報 JSON API
#http://www.p2pquake.com/dev/?q=json-api
sub getMessage
{
	my ($item) = @_;
	
	my $message = "";

	my $time = $item->{"time"};
	my $code = $item->{"code"};

	if($code != 551)
	{
		return "";
	}
	
	my $earthquake = $item->{"earthquake"};
	
	$t = $earthquake->{"time"};
	$t =~ s/[0-9]+日//g;
	$message .= $t . " ";
	
	$hypocenter = $earthquake->{"hypocenter"};
	$t = $hypocenter->{"name"};
	$message .= $t . " ";

	%tbl = 
	(
		0 => "なし",
		10 => "震度1",
		20 => "震度2",
		30 => "震度3",
		40 => "震度4",
		45 => "震度5弱",
		50 => "震度5強",
		55 => "震度6弱",
		60 => "震度6強",
		70 => "震度7",
	);
	
	$maxScale = $earthquake->{"maxScale"};

	if($maxScale == 0)
	{
		return "";
	}

	$t = $tbl{$maxScale};
	$message .= $t . " ";

	$message .= "の地震がありました。";

	return $message;
}

my %hash = ();
my $interval = 90;

while(1)
{
	my $date = Date::Simple->new();
	my $json = "";
	my $curl = new WWW::Curl::Easy;
	
	$date = $date->format('%m/%d');
	
	#my $url = "http://api.p2pquake.com/v1/human-readable";
	my $url = "http://api.p2pquake.com/v1/human-readable?limit=2";
	print sprintf("url:%s GET\n", $url);
	
	$curl->setopt(CURLOPT_URL, $url);
	$curl->setopt(CURLOPT_WRITEDATA, \$json);
	$curl->perform;

	if ($curl->getinfo(CURLINFO_HTTP_CODE) == 200)
	{
		$json = decode("utf-8", $json);
		$arrayRef = JSON->new()->decode($json);
		
		my $hash_size = keys(%hash);

		foreach my $key(keys(%hash))
		{
			$hash{$key}{"delete"} = 1;
		}
		
		foreach my $item(@$arrayRef)
		{
			my $key = $item->{"time"};

			my $message = &getMessage($item);
			#print $message;
			
			if(length($message) == 0)
			{
				next;
			}

			if(!exists($hash{$key}))
			{
				$hash{$key}{"message"} = $message;
				$hash{$key}{"used"} = 0;
			}

			delete($hash{$key}{"delete"});
		}
		
		#初回
		if($hash_size == 0)
		{
			my @keys = sort(keys(%hash));
			
			$hash_size = @keys;
			
			if($hash_size > 0)
			{
				foreach my $key (@keys)
				{
					$hash{$key}{"used"} = 1;
				}

#				my $lastKeyIndex = $#keys;
#
#				if($lastKeyIndex >= 0)
#				{
#					my $key = $keys[$lastKeyIndex];
#					$hash{$key}{"used"} = 0;
#				}
			}
			
			$hash{"system"}{"used"} = 0;
			$hash{"system"}{"message"} = "地震情報監視スクリプトを起動しました。";
		}

		foreach my $key (keys(%hash))
		{
			if
			(
				$hash{$key}{"used"} == 1 
				&& $hash{$key}{"delete"} == 1
			)
			{
				delete($hash{$key});
			}
		}
	}
	
	#print Dumper %hash;

	foreach my $key (sort(keys(%hash)))
	{
		my $message = $hash{$key}{"message"};
		
		if($hash{$key}{"used"} == 0 )
		{
			#$command = sprintf("/home/pi/aquestalkpi/AquesTalkPi '%s' | sox -V0 -t wav - -t wav - gain -l 19 | aplay &", $message);

			#Open JTalk
			#{
			#	 hts_engine API
			#	 http://hts-engine.sourceforge.net/
			#
			#	 Open JTalk
			#	 http://open-jtalk.sourceforge.net/
			#
			#	 OpenJTalkの導入
			#	 http://oohito.com/nqthm/archives/2034
			#
			#	 日本語の文字列をオーディオデータに変換するOpen JTalkのGNU/Linuxへの導入に関する追加メモ(2012年1月中旬時点)
			#	 http://d.hatena.ne.jp/kakurasan/20120114/p1
			#
			#	 MMDAgent
			#	 http://sourceforge.net/projects/mmdagent/files/MMDAgent_Example/
			#
			#	 Open JTalkでメイちゃんにしゃべってもらう
			#	 http://aidiary.hatenablog.com/entry/20131006/1381061297
			#}
			$t = $message;
			$t =~ s/ /、/g;
			$command = "";
			$command .= sprintf("echo '%s'", $t);
			$command .= "| open_jtalk -u 0.55 -r 1.1 -m /usr/local/share/open_jtalk/voice/mei/mei_bashful.htsvoice -x /usr/local/share/open_jtalk/open_jtalk_dic_utf_8-1.07 -ow bashful_temp.wav";

			$command = encode("utf-8", $command);
			print "\n" . $command . "\n";
			system($command);

			$command = "sox -V0 bashful_temp.wav -t wav - gain -l 10 | aplay &";
			print "\n" . $command . "\n";
			system($command);

			$command = sprintf("echo '%s' | sudo /home/pi/display16x32/my-rpi-rgb-led-matrix/led_matrix_16x32_string_screen --input.stdin", $message);

			$command = encode("utf-8", $command);
			print "\n" . $command . "\n";
			system($command);
			
			$hash{$key}{"used"} = 1;
		}
	}
	
	print sprintf("wait %dsec\n", $interval);
	sleep($interval);
}