program name
    led_matrix16x32_string_screen

args
    --input.file=/filepath
    --input.stdin
    --input.pipe=name
    --string.color=0xFF0000
    --string.ignore_controll_character=1
    --help
