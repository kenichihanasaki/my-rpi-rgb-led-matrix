CFLAGS=-Wall -O3 -g
CXXFLAGS=-Wall -O3 -g -std=c++0x -I./libs/freetype-2.5.3/include -I /usr/local/include
OBJECTS=main.o gpio.o led-matrix.o thread.o
BINARIES=led_matrix_16x32_string_screen
LDFLAGS=-lrt -lm -lpthread

all : $(BINARIES)

led-matrix.o: led-matrix.cc led-matrix.h
main.o: led-matrix.h

led_matrix_16x32_string_screen : $(OBJECTS)
	$(CXX) $(CXXFLAGS) $^ -o $@ ./libs/freetype-2.5.3/objs/.libs/libfreetype.so /usr/local/lib/libiconv.so $(LDFLAGS)

clean:
	rm -f $(OBJECTS) $(BINARIES)
