#! /usr/bin/perl

use feature ':5.10';
use utf8;
use Encode;
use WWW::Curl::Easy;

#cpan HTML::TreeBuilder
use HTML::TreeBuilder;

#cpan HTML::TreeBuilder::XPath
use HTML::TreeBuilder::XPath;

#cpan HTML::Selector::XPath
use HTML::Selector::XPath qw/selector_to_xpath/;

my $interval = 60 * 5;

my $html = "";

my $curl = new WWW::Curl::Easy;
my $url = "http://stocks.finance.yahoo.co.jp/stocks/detail/?code=998407.O";

$curl->setopt(CURLOPT_URL, $url);
$curl->setopt(CURLOPT_WRITEDATA, \$html);



while(1)
{
    $html = "";
    $curl->perform;
    
    if($curl->getinfo(CURLINFO_HTTP_CODE) == 200)
    {
        #printf($html);

        my $treeBuilder = HTML::TreeBuilder->new;
        
        $treeBuilder->parse($html);
        $treeBuilder->eof();

        my $tr = $treeBuilder->findnodes(selector_to_xpath("table.stocksTable tr"))->shift;
        
        $stoksPrice = $tr->look_down("class", "stoksPrice")->as_text;
        $change = $tr->look_down("class", "change")->look_down("class", qr/yjMSt/)->as_text;

        $stoksPrice = decode("utf-8", $stoksPrice);
        $change = decode("utf-8", $change);

        $message = sprintf("日経平均株価 %s円 前日比 %s", $stoksPrice, $change);
        
        if(index($change, "+") != -1)
        {
            $option = "--string.color=0x160000";
        }
        else
        {
            $option = "--string.color=0x000016";
        }
        
        $command = sprintf("echo '%s' | sudo /home/pi/display16x32/my-rpi-rgb-led-matrix/led_matrix_16x32_string_screen --input.stdin %s", $message, $option);
        $command = encode("utf-8", $command);

        printf($command . "\n");
        system($command);

        $treeBuilder = $treeBuilder->delete;
    }

    printf("sleep %d seconds\n", $interval);
    sleep($interval);
}
