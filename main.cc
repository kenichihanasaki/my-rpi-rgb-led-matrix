#include "thread.h"
#include "led-matrix.h"

#include <assert.h>
#include <unistd.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <libgen.h>
#include <algorithm>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <exception>
#include <stdexcept>

#include <iostream>
#include <string>
#include <vector>
#include <mutex>

#include <ft2build.h>
#include FT_GLYPH_H

#include <iconv.h>

using namespace std;
#include <map>
#include <queue>

#include <pthread.h>

#include <stdlib.h>

// Base-class for a Thread that does something with a matrix.

volatile bool g_running;

class RGBMatrixManipulator : public Thread
{
public:

	//RGBMatrixManipulator(RGBMatrix *m) : running_(true), matrix_(m)
	RGBMatrixManipulator(RGBMatrix *m) :  matrix_(m)
	{
	}

	virtual ~RGBMatrixManipulator()
	{
		//running_ = false;
	}

	// Run() implementation needs to check running_ regularly.

protected:
	//volatile bool running_; // TODO: use mutex, but this is good enough for now.
	
	RGBMatrix * const matrix_;
};

/**
 * Pump pixels to screen. Needs to be high priority real-time because jitter
 * here will make the PWM uneven.
 */
class DisplayUpdater : public RGBMatrixManipulator
{
public:
	/**
	 * 
	 */
	DisplayUpdater(RGBMatrix *m) : RGBMatrixManipulator(m)
	{
	}

	/**
	 * 
	 */
	void Run()
	{
		while (g_running)
		{
			matrix_->UpdateScreen();
		}
	}
};

/**
 * 
 */
class FreeType2Util
{
private:
	FT_Library library;
	FT_Face face;
public:
	/**
	 * 
	 */
	FreeType2Util()
	{
		int error = FT_Init_FreeType( &library );
		if ( error )
		{
			throw std::runtime_error("FT_Init_FreeType");
		}
	}
	
	/**
	 * 
	 */
	void Load(const char* fontPath, int pixcelWidth, int pixcelHeight)
	{
		int error;
		
		error = FT_New_Face( library, fontPath, 0, &face );
		
		if (error) 
		{
			printf("FT_New_Face\n");
			throw std::runtime_error("FT_New_Face");
		}
		
		error = FT_Set_Pixel_Sizes(face, pixcelWidth, pixcelHeight);
		
		if (error) 
		{
			printf("FT_Set_Pixel_Sizes\n");
			throw std::runtime_error("FT_Set_Pixel_Sizes");
		}
	}
	
	/**
	 * 
	 */
	FT_Face getFt_Face(int charCode)
	{
		int error;
		
		error = FT_Load_Char(face, charCode, 0);
		if (error) 
		{
			printf("FT_Load_Char\n");
			throw std::runtime_error("FT_Load_Char");
		}
		
		return face;
	}
	
	/**
	 * 
	 */
	FT_Bitmap* ToFT_BitmapMonochrome(FT_Face face)
	{
		int error = FT_Render_Glyph(face->glyph, FT_RENDER_MODE_MONO);
		if (error) 
		{
			printf("FT_Render_Glyph\n");
			throw std::runtime_error("FT_Render_Glyph");
		}
		
		//printf("left:%d, top:%d\n", face->glyph->bitmap_left, face->glyph->bitmap_top);
		
		return &face->glyph->bitmap;
	}
};

/**
 * 
 */
class Color
{
public:
	Color()
	{
		Red = 0;
		Green = 0;
		Blue = 0;
	}
	
	unsigned char Red;
	unsigned char Green;
	unsigned char Blue;
};

/**
 * 
 */
class ScrollScreen : public RGBMatrixManipulator
{
private:
	FreeType2Util util;
	std::vector<uint32_t> stringBuffer;
	iconv_t descriptor;
	int offsetX;
	
	Color stringColor;
	int ignoreControlCharacter;
	string currentDir;
	string inputFilePath;
	
	/**
	 * 
	 */
	void SetPixcel(int x, int y, int r, int g, int b)
	{
		const int width = getRGBMatrix()->width();
		const int height = getRGBMatrix()->height();
		
		if(x < 0 || x >= width || y < 0 || y >= height)
		{
			return;
		}
		
		getRGBMatrix()->SetPixel(x, y, r, g, b);
	}
	
	/**
	 * 
	 */
	int printChar(unsigned int charCode, int x0, int y0, int r, int g, int b)
	{
		FT_Face face = util.getFt_Face(charCode);
		FT_Bitmap* bitmap = util.ToFT_BitmapMonochrome(face);

		for (int row = 0; row < bitmap->rows; row ++) 
		{
			for (int col = 0; col < bitmap->pitch; col ++) 
			{
				char c = bitmap->buffer[bitmap->pitch * row + col];

				for (int bit = 7; bit >= 0; bit --) 
				{
					int x  = face->glyph->bitmap_left + (8 * col) + (7 - bit);
					int y  = y0 - face->glyph->bitmap_top + row;
					
					if (((c >> bit) & 1) == 1)
					{
						SetPixcel(x0 + x, y, r, g, b);
					}
					else
					{
						SetPixcel(x0 + x, y, 0, 0, 0);
					}
				}
			}
		}
		
		return face->glyph->metrics.horiAdvance / 64;
	}
protected:
	/**
	 * 
	 */
	RGBMatrix*  getRGBMatrix()
	{
		return matrix_;
	}
public:
	static const int INPUT_FILE = 0x01;
	static const int INPUT_STDIN = 0x02;
	static const int INPUT_PIPE = 0x04;
	
	/**
	 * 
	 */
	ScrollScreen(RGBMatrix *m) : RGBMatrixManipulator(m)
	{
		stringBuffer.clear();
		
		const char* toCode = "UTF-32BE";
		const char* fromCode = "UTF-8";
		
		descriptor = iconv_open(toCode, fromCode);
		
		if(descriptor == (iconv_t)-1)
		{
			printf("iconv error\n");
		}
		
		stringColor.Red = 32;
		stringColor.Green = 32;
		stringColor.Blue = 32;
		
		ignoreControlCharacter = true;
	}
	
	int inputMode;
	
	/**
	 * 
	 */
	int getInputMode()
	{
		return inputMode;
	}
	
	/**
	 * 
	 */
	void setInputMode(int mode)
	{
		inputMode = mode;
	}
	
	/**
	 * 
	 */
	void setInputFilePath(string path)
	{
		inputFilePath = path;
	}
	/**
	 * 
	 */
	string getInputFilePath()
	{
		return inputFilePath;
	}
	
	/**
	 * 
	 */
	void setStringColor(Color color)
	{
		stringColor.Red = color.Red;
		stringColor.Green = color.Green;
		stringColor.Blue = color.Blue;
	}
	
	/**
	 * 
	 */
	void setIgnoreControlCharacter(bool enable)
	{
		ignoreControlCharacter = enable;
	}
	
	/**
	 * 
	 */
	void setCurrentDir(string dir)
	{
		currentDir = dir;
		
		if(currentDir.find("/", currentDir.size() - 1) == string::npos)
		{
			currentDir += "/";
		}
	}
	
	/**
	 * 
	 */
	std::string getCurrentDir()
	{
		return currentDir;
	}
	
	/**
	 * 
	 */
	void loadFont()
	{
//		const char* fontPath = "/home/pi/display16x32/my-rpi-rgb-led-matrix/fonts/misaki_ttf_2012-06-03/misaki_gothic.ttf";
//		util.Load(fontPath, 8, 8);
		
		string fontPath = getCurrentDir() + "fonts/PixelMplus-20130602/PixelMplus12-Regular.ttf";
		util.Load(fontPath.c_str(), 12, 12);
	}
	
	/**
	 * 
	 */
	~ScrollScreen()
	{
		iconv_close(descriptor);
	}
	
	/**
	 * UTF8 to UTF32 arm(little endian?)
	 */
	void ToVector(const std::string& src, std::vector<uint32_t>& dest)
	{
//		dest.push_back(0x3042);
//		dest.push_back(0x3057);
//		dest.push_back(0x305f);
		
		unsigned int size = src.size();
		
		unsigned char c[6];
		
		for(unsigned int i = 0; i < size; i++)
		{
			//dest.push_back(0x3042);
			
			memset(c, 0, sizeof(c));
			
			for(int k = 0; k< 6 && (i + k) < size; k++)
			{
				c[k] = src.at(i + k);
			}
			
			size_t length = 0;
			
			if
			(
				(c[0] & 0xFC) == 0xFC 
				&& (c[1] & 0x80) == 0x80 
				&& (c[2] & 0x80) == 0x80 
				&& (c[3] & 0x80) == 0x80 
				&& (c[4] & 0x80) == 0x80 
				&& (c[5] & 0x80) == 0x80 
			)
			{
				//length = 6;
				i += 5;
			}
			else if
			(
				(c[0] & 0xF8) == 0xF8 
				&& (c[1] & 0x80) == 0x80 
				&& (c[2] & 0x80) == 0x80 
				&& (c[3] & 0x80) == 0x80 
				&& (c[4] & 0x80) == 0x80 
			)
			{
				//length = 5;
				i += 4;
			}
			else if
			(
				(c[0] & 0xF0) == 0xF0 
				&& (c[1] & 0x80) == 0x80 
				&& (c[2] & 0x80) == 0x80 
				&& (c[3] & 0x80) == 0x80 
			)
			{
				length = 4;
			}
			else if
			(
				(c[0] & 0xE0) == 0xE0 
				&& (c[1] & 0x80) == 0x80 
				&& (c[2] & 0x80) == 0x80 
			)
			{
				length = 3;
			}
			else if
			(
				(c[0] & 0xC2) == 0xC2 
				&& (c[1] & 0x80) == 0x80 
			)
			{
				length = 2;
			}
			else if
			(
				(c[0] & 0x00) == 0x00 
			)
			{
				length = 1;
				
				if(ignoreControlCharacter)
				{
					if((c[0] >= 0x00 && c[0] <= 0x1F) || c[0] == 0x7F)
					{
						length = 0;
					}
				}
			}
			
			//printf("length:%d\n", (int)length);
			//printf("dump:%x,%x,%x,%x,%x,%x\n", c[0], c[1], c[2], c[3], c[4], c[5]);
			
			if(length > 0)
			{
				size_t iLen = length;
				size_t oLen = 16;
				uint32_t out = 0;
				
				char inBuffer[4 + 1];
				char outBuffer[16 + 1];
				
				memset(inBuffer, 0, sizeof(inBuffer));
				memset(outBuffer, 0, sizeof(outBuffer));
				
				memcpy(&inBuffer[0], &c[0], length);
				
				char* pIn = inBuffer;
				char* pOut = outBuffer;
				
				if(iconv(descriptor, &pIn,  &iLen, &pOut, &oLen) != (size_t)-1)
				{
					out = outBuffer[0] << 24 | outBuffer[1] << 16 | outBuffer[2] << 8 | outBuffer[3] << 0;
					//printf("0x%x\n", out);
					dest.push_back(out);
				}
				else
				{
					printf("convert error\n");
				}
				
				i += length - 1;
			}
		}
	}
	
	/**
	 * 
	 */
	bool Print(std::string s)
	{
		try
		{
			if(stringBuffer.size() == 0)
			{
				stringBuffer.clear();
				ToVector(s, stringBuffer);
				offsetX = getRGBMatrix()->width();
			}
			
			int baseLine = 13;
			int x = 0;
			int y = baseLine;
			
			int w0 = 0;
			
			for(unsigned int i = 0; i < stringBuffer.size(); i++)
			{
				unsigned int charCode = stringBuffer.at(i);
				
				int w = printChar
				(
					charCode,
					
					x + offsetX,
					y,
					
					stringColor.Red,
					stringColor.Green,
					stringColor.Blue
				);
				
				if(w0 == 0)
				{
					w0 = w;
				}
				
				x += w;
				
				if(getRGBMatrix()->width() < (x + offsetX))
				{
					break;
				}
			}
			
			offsetX--;
			
			if(offsetX < 0 && abs(offsetX) >= w0)
			{
				offsetX = 0;
				stringBuffer.erase(stringBuffer.begin());
				
				if(stringBuffer.size() == 0)
				{
					//printf("stringBuffer.size() == 0\n");
					return false;
				}
			}
		}
		catch(std::runtime_error& e)
		{
			printf("catch exception\n%s\n", e.what());
		}
		
		return true;
	}
	
	/**
	 * 
	 */
	std::string readString(FILE* fp)
	{
		char buffer[1024];
		memset(buffer, 0, sizeof(buffer));
		
		std::string s = "";
		
		while(fgets(buffer , sizeof(buffer),  fp) != NULL) 
		{
			s += buffer;
		}
		
		fclose(fp);
		
		return s;
	}
	
	/**
	 * 
	 */
	bool createFifo(std::string name)
	{
		printf("createFifo:%s\n", name.c_str());
		
		if(access(name.c_str(), W_OK | R_OK) == 0)
		{
		}
		else
		{
			if(mkfifo(name.c_str(), 0777) == -1)
			{
				printf("mkfifo failed");
				return false;
			}
		}
		
		return true;
	}
	
	/**
	 * 
	 */
	void defaultRoutine()
	{
		FILE* fp = NULL;
		
		switch(getInputMode())
		{
			case ScrollScreen::INPUT_FILE:
				printf("inputFilePath:%s\n", getInputFilePath().c_str());
				
				if((fp = fopen(getInputFilePath().c_str(), "r")) == NULL)
				{
					printf("fopen failed:%s\n", getInputFilePath().c_str());
					exit(EXIT_FAILURE);
				}
			break;
            
			case ScrollScreen::INPUT_STDIN:
				fp = stdin;
			break;
			
			case ScrollScreen::INPUT_PIPE:
			{
				std::string pipePath = getInputFilePath();
				
				createFifo(pipePath);
				if((fp = fopen(pipePath.c_str(), "r")) == NULL)
				{
					printf("fopen failed:%s\n", pipePath.c_str());
					exit(EXIT_FAILURE);
				}
			}
			break;
			
			default:
				exit(EXIT_FAILURE);
		}
		
        std::string s = readString(fp);
		printf("%s", s.c_str());
		
		while (g_running)
		{
			switch(getInputMode())
			{
				case ScrollScreen::INPUT_FILE:
				case ScrollScreen::INPUT_STDIN:
					g_running = Print(s);
				break;
                
				case ScrollScreen::INPUT_PIPE:
					if(!Print(s))
					{
						std::string pipePath = getInputFilePath();
						
						if((fp = fopen(pipePath.c_str(), "r")) == NULL)
						{
							printf("fopen failed:%s\n", pipePath.c_str());
							exit(EXIT_FAILURE);
						}
						
						s = readString(fp);
					}
				break;
			}
			
			usleep(40 * 1000);
		}
	}
	
//	std::map<int, std::queue<string>> map;
//	pthread_mutex_t mutex;
//	
//	struct readNamedPipeThreadArgs
//	{
//		int priority;
//		Test* pThis;
//	};
//	
//	/**
//	 * 
//	 */
//	static void* readNamedPipeThread(void* args)
//	{
//		readNamedPipeThreadArgs* p = (readNamedPipeThreadArgs*)args;
//		int priority = p->priority;
//		Test* that = p->pThis;
//		
//		printf("thread[%d] start\n", priority);
//		
//		std::string pipePath = that->getCurrentDir() + "p" + std::to_string(priority);
//		
//		FILE* fp = NULL;
//
//		if((fp = fopen(pipePath.c_str(), "r")) == NULL)
//		{
//			printf("fopen failed:%s\n", pipePath.c_str());
//			exit(EXIT_FAILURE);
//		}
//		
//		string s = that->readString(fp);
//		
//		pthread_mutex_lock(&that->mutex);
//		that->map[priority].push(s);
//		pthread_mutex_unlock(&that->mutex);
//		
//		printf("thread[%d] end\n", priority);
//		
//		return NULL;
//	}
	
	/**
	 * 
	 */
	void Run()
	{
        defaultRoutine();
	}
};

vector<string> parseArgs(int argc, char *argv[])
{
	vector<string> r;
	
	for(int i = 0; i < argc; i++)
	{
		string t = argv[i];
		r.push_back(t);
	}
	
	return r;
}

/**
 * 
 */
void printHelp()
{
	printf("args\n");
	printf("--input.file=/filepath\n");
	printf("--input.stdin\n");
	printf("--input.pipe=name\n");
	printf("--string.color=0xFF0000\n");
	printf("--string.ignore_controll_character=1\n");
	printf("--help\n");
}

/**
 * 
 */
void setUp(ScrollScreen* pScrollScreen, vector<string> args)
{
	char buffer[1024];
	string currentDir = dirname(realpath(args[0].c_str(), buffer));
	
	//printf("currentDir:%s\n", currentDir.c_str());
	pScrollScreen->setCurrentDir(currentDir);
	pScrollScreen->loadFont();
	
	bool found = false;
	
	for(unsigned int i = 0; i < args.size(); i++)
	{
		string arg = args[i];
		printf("args[%d]:%s\n", i, args[i].c_str());
		
		string t;
		unsigned int pos;
		
		t = "--string.color=0x";
		pos = arg.find(t, 0);
		
		if(pos != string::npos)
		{
			 string rgb = arg.substr((t.size() - 1) + 1, 6);
			 
			 printf("rgb:%s\n", rgb.c_str());
			 
			 string t = "";
			 
			 t = "0x" + rgb.substr(0, 2);
			 int red = stoi(t, NULL, 16);
			 
			 t = "0x" + rgb.substr(2, 2);
			 int green = stoi(t, NULL, 16);
			 
			 t = "0x" + rgb.substr(4, 2);
			 int blue = stoi(t, NULL, 16);
			 
			 Color stringColor;
			 stringColor.Red = (unsigned char)red;
			 stringColor.Green = (unsigned char)green;
			 stringColor.Blue = (unsigned char)blue;
			 
			 pScrollScreen->setStringColor(stringColor);
		}
		
		t = "--string.ignore_controll_character=";
		pos = arg.find(t, 0);
		
		if(pos != string::npos)
		{
			 t = arg.substr((t.size() - 1) + 1, 1);
			 bool enable = (atoi(t.c_str()) == 1);
			 pScrollScreen->setIgnoreControlCharacter(enable);
		}
		
		t = "--input.file=";
		pos = arg.find(t, 0);
		
		if(pos != string::npos)
		{
			if(found)
			{
				exit(EXIT_FAILURE);
			}
			
			t = arg.substr((t.size() - 1) + 1, arg.size() - t.size());
			t = realpath(t.c_str(), buffer);
			pScrollScreen->setInputMode(ScrollScreen::INPUT_FILE);
			pScrollScreen->setInputFilePath(t);
			found = true;
		}
		
		t = "--input.stdin";
		pos = arg.find(t, 0);
		
		if(pos != string::npos)
		{
			if(found)
			{
				exit(EXIT_FAILURE);
			}
			
			pScrollScreen->setInputMode(ScrollScreen::INPUT_STDIN);
			found = true;
		}
		
		t = "--input.pipe=";
		pos = arg.find(t, 0);
		
		if(pos != string::npos)
		{
			if(found)
			{
				exit(EXIT_FAILURE);
			}
			
			t = arg.substr((t.size() - 1) + 1, arg.size() - t.size());
			
			pScrollScreen->setInputMode(ScrollScreen::INPUT_PIPE);
			pScrollScreen->setInputFilePath(t);
			found = true;
		}
		
		t = "--help";
		pos = arg.find(t, 0);
		if(pos != string::npos)
		{
			printHelp();
			exit(0);
		}
	}
	
	if(!found)
	{
		printHelp();
		exit(EXIT_FAILURE);
	}
}

/**
 * 
 */
int main(int argc, char *argv[])
{
	vector<string> args = parseArgs(argc, argv);
	
	GPIO io;
	if (!io.Init())
		return 1;
	
	RGBMatrix m(&io);

	RGBMatrixManipulator *image_gen = NULL;
	
	image_gen = new ScrollScreen(&m);
	setUp(((ScrollScreen*)image_gen), args);
	
	RGBMatrixManipulator *updater = new DisplayUpdater(&m);
	
	g_running = true;
	
	updater->Start(10); // high priority
	image_gen->Start();

	while(g_running)
	{
		usleep(1000);
	}
	
	// Stopping threads and wait for them to join.
	delete image_gen;
	delete updater;

	// Final thing before exit: clear screen and update once, so that
	// we don't have random pixels burn
	m.ClearScreen();
	m.UpdateScreen();
	
	return 0;
}
